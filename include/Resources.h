#ifndef __RESOURCES__
#define __RESOURCES__

#include "olcPixelGameEngine.h"

class Resources
{
public:
	Resources() = delete;
	
	static olc::Renderable item_sprite_sheet;
	static olc::Renderable item_highlight;
};

#endif
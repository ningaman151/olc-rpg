#ifndef __UI__
#define __UI__

#include <string>
#include "olcPixelGameEngine.h"
#include "Instance.h"

#define BUTTON_WIDTH 100
#define BUTTON_HEIGHT 30
#define BUTTON_SEPARATION_Y 20

class MouseButton : public Instance
{
public:
	float pos_x, pos_y;
	float width, height;
	std::string text;
	
	bool hovered = false;
	
	std::function<void()> action;
	
	olc::Pixel normal_color = olc::WHITE;
	olc::Pixel hover_color = olc::DARK_GREY;
	
	olc::Pixel color = normal_color;
	
	MouseButton(float _x, float _y, float _width, float _height, std::string _text, std::function<void()> _action, olc::Pixel _normal_color = olc::WHITE, olc::Pixel _hover_color = olc::DARK_GREY);
	MouseButton(float _x, float _y, std::string _text, std::function<void()> _action, olc::Pixel _normal_color = olc::WHITE, olc::Pixel _hover_color = olc::DARK_GREY);
	~MouseButton();

	virtual void update(olc::PixelGameEngine* engine, float delta_time) override;
	virtual void draw(olc::PixelGameEngine* engine) override;
	//bool interact(olc::PixelGameEngine* engine);
};

#endif
#ifndef __CHARACTERS__
#define __CHARACTERS__

#include <string>
#include "olcPixelGameEngine.h"
#include "Instance.h"
#include "Items.h"
#include "Inventory.h"

class Character : public Instance
{
public:
	Character(std::string _name);
	
	std::string name;
	unsigned level = 1;
	olc::Renderable sprite;
	
	virtual void update(olc::PixelGameEngine* engine, float delta_time) override;
	virtual void draw(olc::PixelGameEngine* engine) override;
};

class PlayingCharacter : public Character
{
public:
	PlayingCharacter(std::string _name, int _hp, float _x, float _y);
	
	int hp;
	float x, y;
};

class Player : public PlayingCharacter
{
public:
	Player(std::string _name, int _hp, float _x, float _y, olc::PixelGameEngine* engine);
	void update(olc::PixelGameEngine* engine, float delta_time) override;
	void draw(olc::PixelGameEngine* engine) override;
	//void draw_inventory(olc::PixelGameEngine* engine);
	
	int damage = 1;
	int gold = 5;
	int exp = 0;
	int8_t main_equipped_slot = -1;
	
	Inventory inventory;
	//std::array<Item*, NUM_PLAYER_INV_SLOTS> inventory;
};

class Enemy : public PlayingCharacter
{
public:
	Enemy(std::string _name, int _hp, int _gold_value, float _x, float _y, unsigned _level);
	
	int gold_value;
};

#endif
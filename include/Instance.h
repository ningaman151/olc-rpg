#ifndef __INSTANCE__
#define __INSTANCE__

#include "olcPixelGameEngine.h"

class Instance
{
public:
	virtual void update(olc::PixelGameEngine* engine, float delta_time) = 0;
	virtual void draw(olc::PixelGameEngine* engine) = 0;
};

#endif
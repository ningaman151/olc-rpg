#ifndef __RPGgame__
#define __RPGgame__

#include <string>
#include <list>
#include "olcPixelGameEngine.h"
#include "Characters.h"
#include "Page.h"

class RPGgame : public olc::PixelGameEngine
{
public:
	//Figure out where to put characters
	//std::list<std::unique_ptr<Character>> active_instances;
	Page main_menu, options_menu, level_1;

	Page* active_page = nullptr;
	
	std::shared_ptr<Player> player = nullptr;
	//Player player;
	
	RPGgame();
	bool OnUserCreate() override;
	bool OnUserUpdate(float) override;
};

#endif

//std::list<std::unique_ptr<Character>> active_instances;
//std::list<Character*> active_instances;
#ifndef __INVENTORY__
#define __INVENTORY__

#include "Items.h"

#define NUM_PLAYER_INV_SLOTS 8
#define INVENTORY_WIDTH NUM_PLAYER_INV_SLOTS * ITEM_WIDTH

class InventorySlot
{
public:	
	Item* slot_item = nullptr;
	bool hovered = false;
	bool highlighted = false;
	uint16_t pos_x, pos_y;
	
	InventorySlot() = default;
	InventorySlot(uint16_t _pos_x, uint16_t _pos_y);
	
	void update(olc::PixelGameEngine* engine, float delta_time);
	void draw(olc::PixelGameEngine* engine);
};

class Inventory
{
public:
	enum class ItemType
	{
		Potion,
		Shield,
		Sword
	};
	
	std::array<InventorySlot, NUM_PLAYER_INV_SLOTS> slots;
	
	Inventory(olc::PixelGameEngine* engine);
	
	void update(olc::PixelGameEngine* engine, float delta_time);
	void draw(olc::PixelGameEngine* engine);
	
	Item* add_item(ItemType type, std::function<void(size_t index)> onUse);
	Item* add_item(size_t slotNumber, ItemType type, std::function<void(size_t index)> onUse); // add in specific slot
	void remove_item(uint8_t index);
};

#endif

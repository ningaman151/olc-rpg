#ifndef __PAGE__
#define __PAGE__

#include "olcPixelGameEngine.h"
#include "Instance.h"

class Page 
{
public:
	std::list<std::shared_ptr<Instance>> instances;
	
	//Page();
	void load_instances(std::list<std::shared_ptr<Instance>>);
	
	void update(olc::PixelGameEngine* engine, float delta_time);
	void draw(olc::PixelGameEngine* engine);
};

#endif 

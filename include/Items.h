#ifndef __ITEMS__
#define __ITEMS__

#include <string>
#include "olcPixelGameEngine.h"

#define ITEM_WIDTH 34
#define ITEM_HEIGHT 34

class Item
{
public:
	std::string name;
	olc::vi2d sprite_pos;
	
	Item(std::string, olc::vi2d);
	virtual void select();
};

class EquippableItem : public Item
{
public:
	EquippableItem(std::string, olc::vi2d, std::function<void()>);
	
	std::function<void()> action;
	
	void select() override;
};

class ConsumableItem : public Item
{
public:
	ConsumableItem(std::string, olc::vi2d, std::function<void()>);
	
	std::function<void()> action;
	
	void select() override;
};

#endif
#include <string>
#include "../include/RPGgame.h"
#include "../include/UI.h"
#include "../include/Resources.h"


RPGgame::RPGgame()
{
	sAppName = "RPG game";
}

bool RPGgame::OnUserCreate()
{
	player = std::make_shared<Player>("Nabil", 10, 100.0F, 100.0F, this); //std::make_shared<Player>("Nabil", 10, 0, 0);
	
	(player->sprite).Load("./gfx/player.png");
	
	Resources::item_sprite_sheet.Load("./gfx/item_spritesheet.png");
	Resources::item_highlight.Load("./gfx/selected.png");
	
	
	int spacing_y = 20;
	
	int pos_x = ScreenWidth() / 2 - BUTTON_WIDTH / 2;
	int pos_y_1 = ScreenHeight() / 2 - BUTTON_HEIGHT / 2;
	
	int pos_y_2 = pos_y_1 + BUTTON_HEIGHT + BUTTON_SEPARATION_Y;
	
	//std::list<Instance*> main_menu_elements = {
	//	//std::make_unique<Instance>(MouseButton(pos_x, pos_y_1, "Hello World", void_func)),
	//	new MouseButton(pos_x, pos_y_1, "Hello World", void_func),
	//	new MouseButton(pos_x, pos_y_2, "Test", void_func)
	//};
	
	auto goto_level_1 = [&]()
	{
		active_page = &level_1;
	};
	
	auto goto_options_menu = [&]()
	{
		active_page = &options_menu;
	};
	
	std::list<std::shared_ptr<Instance>> main_menu_elements = {
		std::static_pointer_cast<Instance>(std::make_shared<MouseButton>(pos_x, pos_y_1, "Start", goto_level_1)),
		std::static_pointer_cast<Instance>(std::make_shared<MouseButton>(pos_x, pos_y_2, "Options", goto_options_menu))
	};
	
	main_menu.load_instances(main_menu_elements);
	
	/*auto test_remove = [&]()
	{
		main_menu.instances.erase(main_menu.instances.begin());
	};*/
	
	auto goto_main_menu = [&]()
	{
		active_page = &main_menu;
	};
	
	std::list<std::shared_ptr<Instance>> test_menu_elements = {
		std::static_pointer_cast<Instance>(std::make_shared<MouseButton>(MouseButton(pos_x, pos_y_1, "Option 1", nullptr))),
		std::static_pointer_cast<Instance>(std::make_shared<MouseButton>(MouseButton(pos_x, pos_y_2, "Back", goto_main_menu)))
	};
	
	options_menu.load_instances(test_menu_elements);
	
	std::list<std::shared_ptr<Instance>> level_1_elements = {
		std::static_pointer_cast<Instance>(player)
	};
	
	level_1.load_instances(level_1_elements);
	
	active_page = &main_menu;
	
	return true;
}

bool RPGgame::OnUserUpdate(float fElapsedTime)
{	
	Clear(olc::BLACK);
	
	active_page->update(this, fElapsedTime);	
	active_page->draw(this);
	
	return true;
}
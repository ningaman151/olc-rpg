#include "../include/UI.h"

MouseButton::MouseButton(float _x, float _y, float _width, float _height, std::string _text, std::function<void()> _action, olc::Pixel _normal_color, olc::Pixel _hover_color) :
	pos_x(_x), pos_y(_y), width(_width), height(_height), text(_text), action(_action), normal_color(_normal_color), hover_color(_hover_color) {}

MouseButton::MouseButton(float _x, float _y, std::string _text, std::function<void()> _action, olc::Pixel _normal_color, olc::Pixel _hover_color) :
	pos_x(_x), pos_y(_y), width(BUTTON_WIDTH), height(BUTTON_HEIGHT), text(_text), action(_action), normal_color(_normal_color), hover_color(_hover_color) {}



void MouseButton::draw(olc::PixelGameEngine* engine)
{
	engine->FillRect(pos_x, pos_y, width, height, color);
	
	olc::vi2d text_size = engine->GetTextSize(text);
	engine->DrawString(pos_x + width / 2 - text_size.x / 2, pos_y + height / 2 - text_size.y / 2, text, olc::BLACK);
}

void MouseButton::update(olc::PixelGameEngine* engine, float delta_time)
{
	bool entered_button = false;
	int mouse_x = engine->GetMouseX();
	int mouse_y = engine->GetMouseY();

	bool bounded_x = mouse_x >= pos_x and mouse_x <= (pos_x + width);
	bool bounded_y = mouse_y >= pos_y and mouse_y <= (pos_y + height);

	if (bounded_x and bounded_y)
	{
		color = hover_color;
		hovered = true;
	}
	else
	{
		color = normal_color;
		hovered = false;
	}

	if (engine->GetMouse(0).bReleased and hovered)
	{
		action();
	}
}

MouseButton::~MouseButton()
{

}
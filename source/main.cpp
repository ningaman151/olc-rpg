#include "../include/olcPixelGameEngine.h"
#include "../include/RPGgame.h"
#include <string>

int main()
{
	RPGgame game;

	if (game.Construct(480, 270, 2, 2))
		game.Start();

	return 0;
}
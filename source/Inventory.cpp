#include "../include/olcPixelGameEngine.h"
#include "../include/Inventory.h"
#include "../include/Items.h"
#include "../include/Resources.h"

InventorySlot::InventorySlot(uint16_t _pos_x, uint16_t _pos_y) : pos_x(_pos_x), pos_y(_pos_y) {}

void InventorySlot::draw(olc::PixelGameEngine* engine)
{
	if (slot_item)
	{
		engine->DrawPartialDecal({ float(pos_x), float(pos_y) }, Resources::item_sprite_sheet.Decal(), olc::vf2d(slot_item->sprite_pos) * ITEM_WIDTH,
			{ ITEM_WIDTH, ITEM_HEIGHT });	
		
		if (hovered)
		{
			engine->DrawString(pos_x, pos_y - 20, slot_item->name);
		}
		
		if (highlighted)
		{
			engine->DrawDecal({ float(pos_x), float(pos_y) }, Resources::item_highlight.Decal());
		}
	}
	else
	{
		engine->FillRect(pos_x, pos_y, ITEM_WIDTH, ITEM_HEIGHT, olc::DARK_BLUE);
	}
}

void InventorySlot::update(olc::PixelGameEngine* engine, float delta_time)
{
	uint32_t mouse_x = engine->GetMouseX();
	uint32_t mouse_y = engine->GetMouseY();
	
	bool bounded_x = mouse_x >= pos_x and mouse_x <= (pos_x + ITEM_WIDTH);
	bool bounded_y = mouse_y >= pos_y and mouse_y <= (pos_y + ITEM_HEIGHT);
	
	hovered = bounded_x and bounded_y;
	
	if (hovered and slot_item != nullptr)
	{
		if (engine->GetMouse(0).bReleased)
		{
			slot_item->select();
		}
	}
}

Inventory::Inventory(olc::PixelGameEngine* engine)
{
	size_t pos = 0;
	
	for (auto& itm : slots)
	{
		auto pos_x = uint32_t(float(engine->ScreenWidth()) / 2.f - INVENTORY_WIDTH / 2.f + pos * ITEM_WIDTH);
		auto pos_y = uint32_t(float(engine->ScreenHeight()) * 0.8f);
		
		itm = InventorySlot(pos_x, pos_y);
		
		++pos;
	}
}

void Inventory::update(olc::PixelGameEngine* engine, float delta_time)
{
	for (auto& slot : slots)
	{
		slot.update(engine, delta_time);
	}
}

void Inventory::draw(olc::PixelGameEngine* engine) {
	
	for (auto& item : slots)
	{
		item.draw(engine);
	}
	
	//size_t pos = 0;
	//
	//for (auto& itm : slots) 
	//{
	//	auto pos_x = uint32_t(float(engine->ScreenWidth()) / 2.f - INVENTORY_WIDTH / 2.f + pos * ITEM_WIDTH),
	//		pos_y = uint32_t(float(engine->ScreenHeight()) * 0.8f);
	//	
	//	if (itm.slot_item != nullptr) 
	//	{ 
	//		engine->DrawPartialDecal({ float(pos_x), float(pos_y) }, Resources::item_sprite_sheet.Decal(),
	//			olc::vf2d(itm.slot_item->sprite_pos) * ITEM_WIDTH, { ITEM_WIDTH, ITEM_HEIGHT });
	//	}
	//	
	//	++pos;
	//}
}

Item* Inventory::add_item(size_t slotNumber, ItemType type, std::function<void(size_t index)> onUse)
{
	auto call_back = std::bind(onUse, slotNumber);
	
	Item* rval = nullptr;
	
	if (slotNumber < NUM_PLAYER_INV_SLOTS and slots[slotNumber].slot_item == nullptr) // not out of bounds and slot is un-used
	{	
		switch (type) 
		{
			case ItemType::Potion: rval = new ConsumableItem("Health Potion", { 0,2 }, call_back); break;
			case ItemType::Shield: rval = new EquippableItem("Shield", { 2,7 }, call_back); break;
			case ItemType::Sword: rval = new EquippableItem("Sword", { 0,3 }, call_back); break;
		}
	}
	
	if (rval != nullptr) 
	{
		slots[slotNumber].slot_item = rval;
	}
	return rval;
}

Item* Inventory::add_item(ItemType type, std::function<void(size_t index)> onUse) {
	for (size_t i = 0; i < slots.size(); ++i) {
		if (slots[i].slot_item == nullptr) return add_item(i, type, onUse); // find emoty slot
	}
	return nullptr; // no free slots
}

void Inventory::remove_item(uint8_t index)
{
	if (slots[index].slot_item != nullptr)
	{
		delete slots[index].slot_item;
		slots[index].slot_item = nullptr;
	}
}
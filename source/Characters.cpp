#include "../include/Characters.h"
#include <string>
#include "../include/Resources.h"

Character::Character(std::string _name) : name(_name) {}

PlayingCharacter::PlayingCharacter(std::string _name, int _hp, float _x, float _y) : Character(_name), hp(_hp), x(_x), y(_y) {}

Player::Player(std::string _name, int _hp, float _x, float _y, olc::PixelGameEngine* engine) : PlayingCharacter(_name, _hp, _x, _y), inventory(engine)
{
	inventory.add_item(Inventory::ItemType::Potion, [&](size_t index)
		{
			hp += 3;
			inventory.remove_item(index);
		});
	
	inventory.add_item(Inventory::ItemType::Sword, [&](size_t index)
		{
			if (main_equipped_slot == index)
			{
				inventory.slots[index].highlighted = false;
				damage -= 2;
				main_equipped_slot = -1;
			}
			else if (main_equipped_slot == -1)
			{
				inventory.slots[index].highlighted = true;
				damage += 2;
				main_equipped_slot = index;
			}
		});
	
	inventory.add_item(Inventory::ItemType::Potion, [&](size_t index)
		{
			hp += 3;
			inventory.remove_item(index);
		});
	
	inventory.add_item(Inventory::ItemType::Sword, [&](size_t index)
		{
			if (main_equipped_slot == index)
			{
				inventory.slots[index].highlighted = false;
				damage -= 2;
				main_equipped_slot = -1;
			}
			else if (main_equipped_slot == -1)
			{
				inventory.slots[index].highlighted = true;
				damage += 2;
				main_equipped_slot = index;
			}
		});
	
	//for (Item*& slot : inventory) slot = nullptr; // new Item("test", { 0, 2 });
	//
	////Create item callbacks here
	//Item* potion = new ConsumableItem{ "Health Potion", {0, 2} };
	////Item* sword = new EquippableItem{ "Sword", {0, 3} };
	//Item* shield = new EquippableItem{ "Shield", {2, 7}, [&]()
	//	{
	//		//printf("We are here");
	//} };
	//
	//inventory[0] = potion;
	//inventory[1] = shield;
}

Enemy::Enemy(std::string _name, int _hp, int _gold_value, float _x, float _y, unsigned _level) : PlayingCharacter(_name, _hp, _x, _y), gold_value(_gold_value)
{
	level = _level;
}

void Character::draw(olc::PixelGameEngine* engine)
{
	engine->DrawDecal({ 100.0f, 100.0f }, sprite.Decal());
}

void Character::update(olc::PixelGameEngine* engine, float delta_time)
{
	//pass
}

void Player::update(olc::PixelGameEngine* engine, float delta_time)
{
	inventory.update(engine, delta_time);
}

void Player::draw(olc::PixelGameEngine* engine)
{
	Character::draw(engine);
	
	inventory.draw(engine);
	
	//Draw stats
	std::string stats_str = "hp: " + std::to_string(hp) + "\n";
	stats_str += "damage: " + std::to_string(damage) + "\n";
	
	engine->DrawString(25, 25, stats_str);
}


#include "../include/Page.h"

void Page::draw(olc::PixelGameEngine* engine)
{
	for (auto instance : instances)
	{
		instance->draw(engine);
	}
}

void Page::update(olc::PixelGameEngine* engine, float delta_time)
{
	for (auto instance : instances)
	{
		instance->update(engine, delta_time);
	}
}

void Page::load_instances(std::list<std::shared_ptr<Instance>> _instances)
{
	instances = _instances;
}
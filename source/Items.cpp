#include "../include/Items.h"

Item::Item(std::string _name, olc::vi2d _sprite_pos) : name(_name), sprite_pos(_sprite_pos) {}

EquippableItem::EquippableItem(std::string _name, olc::vi2d _sprite_pos, std::function<void()> _action) : Item(_name, _sprite_pos), action{ _action } {}

ConsumableItem::ConsumableItem(std::string _name, olc::vi2d _sprite_pos, std::function<void()> _action) : Item(_name, _sprite_pos), action{ _action } {}

void Item::select() {}

void EquippableItem::select()
{
	//Equip item
	action();
}

void ConsumableItem::select()
{
	//Consume item
	//Delete if consumed
	//delete this;
	//printf("We are here");
	action();
}